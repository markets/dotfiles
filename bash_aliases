alias df='df -h' # humanize sizes
alias cp='cp -i' # confirm before overwrite
alias hi='history | tail -20'
alias reload!='. ~/.bash_profile'
alias localip='ipconfig getifaddr en1'
alias pubkey="more ~/.ssh/id_rsa.pub | pbcopy | echo '=> Public key copied to clipboard.'"
alias hosts='more /etc/hosts'

alias c='pbcopy'
alias p='pbpaste'

alias psa='ps aux'
alias psg='ps aux | grep $1'

alias ka9='killall -9'
alias k9='kill -9'

alias l='ls -laGFh'
alias ..='cd ..'
alias ...='cd ../..'
alias cdd='cd -'

alias tf='tail -f'
alias tlog='tail -f log/development.log'

alias post='curl -i -X POST'
alias get='curl -i -X GET'

alias gst='git status'
alias gco='git checkout'
alias gd='git diff'
alias gb='git branch'
alias gs='git show'

alias rs='rails s'
alias rc='rails c'
alias rg='rails g'
alias rd='rails d'

alias gemi='gem install --no-ri --no-rdoc'
alias gemu='gem uninstall'

alias b='bundle'
alias be='bundle exec'
alias bi='bundle install'
alias bu='bundle update'

alias rr='bundle exec rake routes'
alias rt='bundle exec rake -T --all'

alias jks='jekyll server -w'
alias jkb='jekyll build'